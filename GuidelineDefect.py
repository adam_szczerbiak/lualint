class GuidelineDefect:
    """ Aggregates all the information about a certain defect
        (non-conformance with Lua Coding Guidelines).

        fields:
         * description : a string containing the description of the transgression.
         * filename : a string containing the filename of the file in which the transgression occurred.
         * line_number : an int containing the line number where the transgression occurred.
         * char_number : an int containing the column number where the transgression occurred.
         * guideline_reference: a string containing the code of the offended Lua Guideline.
    """

    def __init__(self, description, guideline_reference=None, filename=None,
                line_number=None, char_number=None):
        """ Initializes a new instance of a GuidelineDefect class.
        """
        self.description = description
        self.filename = filename
        self.guideline_reference = guideline_reference
        self.line_number=line_number
        self.char_number = char_number


    def to_string(self):
        """ Converts this instance to a string for the purpose of logging the defects.
        """
        result = ""
        if self.filename is not None:
            result += self.filename
            if self.line_number is not None:
                result += " #{:d}".format(self.line_number)
                if self.char_number is not None:
                    result += ":{:d}".format(self.char_number)

        if result != "":
            result += " "
        # The description parameter is mandatory, it will always be there:
        result += self.description

        if self.guideline_reference is not None:
            result += " [{:s}]".format(self.guideline_reference)

        return result

