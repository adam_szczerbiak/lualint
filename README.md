# LuaLint
This repository contains an attempt at creating an automated tool for checking
conformance with [in-house Lua guidelines](https://wiki.huuuge.net/index.php?title=File:LuaCodingGuidelines.pdf).
This document will be simply referred to as the Guidelines Document below.

### Requirements
Python3 is required to run the code.  
The code has been tested on Linux and Windows (under Cygwin) systems.

### Usage
`python3 lua_lint.py REPO_ROOT_PATH`, where REPO_ROOT_PATH is the path
to the directory containing all the codebase to be checked.
During the process of checking the codebase the progress is logged
to the standard output. The detected issues are buffered,
and logged to a file after the linting is done.

### Example
`python3 ~/Repos/MAGICAS_alpha/res` results in the following output:
```
[1] [16:19:51] simba in ~/Source/LuaLint
$ python3 lua_lint.py ~/Repos/MAGICAS_alpha/res
linting file /home/simba/Repos/MAGICAS_alpha/res/language-list-zh-tw.lua (1/986) ...
linting file /home/simba/Repos/MAGICAS_alpha/res/login.lua (2/986) ...
[ 982 lines of similar output ]
linting file /home/simba/Repos/MAGICAS_alpha/res/baccarat/howtoplay.lua (985/986) ...
linting file /home/simba/Repos/MAGICAS_alpha/res/rateus/rateus.lua (986/986) ...

Done!
5916 detected issues have been logged to file issues.txt
```
The issues are logged to file `issues.txt` in the following format:
```
<filename> #<line-number>:<char-number> <description> <offended-guideline>
```
Some of the fields described above may not be logged for certain issues.
For example, a report of missing terminating newline for a file does not
provide a line number nor a char number, as this would be redundant information.  
The contents of the file `issues.txt` should be similar to the following:
```
Found 5916 issues in total for codebase /home/simba/Repos/MAGICAS_alpha/res :

/home/simba/Repos/MAGICAS_alpha/res/language-list-zh-tw.lua Does not have a line-break character at its end. [2.9]
/home/simba/Repos/MAGICAS_alpha/res/login.lua #114:24 Opening parenthesis is not followed by a space. [2.4.1]
  -- [[ 5911 other issues are not shown in this preview ]] --
/home/simba/Repos/MAGICAS_alpha/res/rateus/rateus.lua #58:110 Closing parenthesis is not preceded by a space. [2.4.1]
/home/simba/Repos/MAGICAS_alpha/res/rateus/rateus.lua #61:79 Opening parenthesis is not followed by a space. [2.4.1]
/home/simba/Repos/MAGICAS_alpha/res/rateus/rateus.lua #61:103 Closing parenthesis is not preceded by a space. [2.4.1]
```

# Coding Guidelines
This section documents the rationale behind every rule:

 - applied to
 - to-be-applied to
 - deliberately omitted from 

this linter.  
The tool checks for compliance with the following rules,
taken directly from the Guidelines Document:

#### 2.1 The code needs to be properly indented
(This is implemented in Rules #2.3.1 & #2.3.2)

#### 2.2 If a line is too long do not be afraid to break it
Long lines warning will be generated for every line exceeding a pre-set characters length. The default value is 150 characters, and it is held in **MAX_ACCEPTED_LINE_LENGTH** variable in *ll_functions.py* module. As the Guidelines Document does not specify the exact meaning of `too long` , feel free to modify it according to your preferences. In my opinion the sane range has an upper bound of 120-150 characters.

#### 2.3 Don't use tabs for indentation. Use four spaces instead.
##### 2.3.1 Do not use tabs for indentation
Warning will be ensued for every line that has at least one tab at the beginning of a line, optionally prefixed by some other whitespace characters. This matches indentation using mixed tabs/spaces, but allows for tab characters used in the code for purposes other than indentation (which is good, as will not generate warnings for appropriate uses of tabs; e.g. using literal tab characters when handling CSV files).

##### 2.3.2 Use indentation of exactly four spaces per each level
To be done - this probably requires writing a lexer to be able to deduct current indentation level.

#### 2.4 Use spacing when specifying parameters.
The Guideline specifically requires that the rules from this section are applied to the entire code:
```
Use this rule in all applicable places (e.g. when writing comments or specifying tables).
```

##### 2.4.1 Apply space character after open parentheses and before closing parentheses
If the parameters' list contains at least one parameter, the parameters have to be separated from the enclosing parentheses by at least one space character. This rule does not apply for open parentheses terminating the line nor for closing parentheses opening the line. 

##### 2.4.2 Always put a space character after a comma
The only exception to this rule is a comma that terminates a line - in this case the space-character is not allowed.

The entire Rule 2.4 means that the following code is valid:
```
foo()
bar( 10, 20, 30 )
baz(
    10,
    20,
    30
)
```
and the following is not:
```
foo(10)
bar( 10,20,30 )
baz( 10,
    20, 30)
```

#### 2.5 Use vertical spacing when switching between logical code groups
**NOT CHECKED**

#### 2.6 But do not overdo the vertical spacing.
**NOT CHECKED**

#### 2.7 Remember to put the newline character at the end of the file.
This is checked first. If the last line in a file does not end with a newline character, a warning is generated.

#### 2.8 Write code that is self-documenting
**NOT CHECKED**

#### 2.9 Use CamelCase [sic] for function names. Use camelCase for variable names.
**LEXER REQUIRED**
This rule will be divided into two sub-rules:
#####2.9.1 Use PascalCase for function names.
##### 2.9.2 Use camelCase for variable names.

#### 2.10 Variable names should be short, but descriptive
**NOT CHECKED**

#### 2.11 Do not make spelling mistakes in variable names, functions and comments.
**LEXER REQUIRED**
It is possible to check every variable/function name against a dictionary. It is also possible to include words from the comments in such check. This may or may not work well, though.

#### 2.12 Do not use brackets [sic] for the outermost level of an if statement.
**LEXER REQUIRED**
The author most certainly meant not to use the parentheses. Anyway, this requires the ability to tell which part of the code is the conditional of an if-statement.

#### 2.13 Keep functions short and to the point. Avoid deeply nested logic. Split functions into multiple smaller ones, if necessary.
**NOT CHECKED**

#### 2.14 Omit semicolons at the end of statements
**LEXER REQUIRED**

#### 2.15 File names should be consistent with each other
**NOT CHECKED**

### What is the meaning of "LEXER REQUIRED" label?
Some of the guidelines above apply only to the certain elements of Lua syntax. In order to be able to enforce them, a linter must be able to parse such syntax. I plan to implement such a lexer/parser in the near future. The rules requiring syntactical/lexical parsing of code have been marked with "**LEXER REQUIRED**" label. They are not implemented yet, but will be in the foreseeable future.

### Why so many of these guidelines are intentionally not checked?
Some of the guidelines apply not to the structure of the code itself,
but rather to the underlying logic or even certain intents behind the code.
It would be a rather hard task to enforce such rules by a linting program.
It would require to write a parser able not only to parse & check the code,
but also to understand the logic and intent behind it as well.
On the other hand, with [recent](https://deepart.io/)
advances
in ML one may be tempted to think that the singularity is just
around the corner.
Feel free to PR me your implementation if you happen to bump into it.
