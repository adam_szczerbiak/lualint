#!/usr/bin/env python3

from ll_functions import *
import sys

def lua_lint_main():
    if len(sys.argv) < 2:
        raise RuntimeError("Please provide a root directory of your Lua files.")
    codebase_dir = sys.argv[1]
    files_list = search_for_files(codebase_dir, ".lua")
    total_files_count = len(files_list)

    all_the_issues = []

    counter = 0
    for entry in files_list:
        counter += 1
        print("linting file {:s} ({:d}/{:d}) ...".format(entry, counter, total_files_count))
        result = lint_file(entry)

        for line in result:
            all_the_issues.append(line)

    print()
    print("Done!")

    outfile = open(LOG_FILENAME, "w")
    outfile.write("Found {:d} issues in total for codebase {:s} :\n\n".format(
            len(all_the_issues), codebase_dir))
    for entry in all_the_issues:
        outfile.write("{:s}\n".format(entry))
    outfile.close()
    print("{:d} detected issues have been logged to file {:s}".format(
            len(all_the_issues), LOG_FILENAME))

if __name__ == "__main__":
    lua_lint_main()
