--[[ What a messy LUA code!
--]]

-- Does not conform to $2.4 at all:
local function foo(param1,param2)

-- Different indent levels:
    bar()
  bar()
-- tabs used for indentation:
	bar()
-- mixed tabs+spaces:
	  bar()
-- Multiple use of tabs for indentation (here: 3 tabs) should be reported once:
			bar()

-- this is a valid syntax, it should not be flagged:
local result = baz(
    1, 2, 3
)
end

-- THIS IS A REALLY LONG LINE THIS IS A REALLY LONG LINE THIS IS A REALLY LONG LINE THIS IS A REALLY LONG LINE THIS IS A REALLY LONG LINE THIS IS A REALLY LONG LINE
