# LuaLint's functions.

from GuidelineDefect import GuidelineDefect

import os
import re # for regular expressions

MAX_ACCEPTED_LINE_LENGTH = 150
INCLUDE_GUIDELINES_REFERENCE=True
LOG_FILENAME="issues.txt"


def search_for_files(directory, extension):
    """ Traverses the given directory searching for files
        ending with a specified extension.
        Returns a list of all the files found in a specified directory.
    """
    result = []
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(extension):
                full_path = os.path.join(root, file)
                result.append(full_path)

    return result


def lint_file(filename):
    """ Inspects the contents of a provided file and reports all the defects.
    """
    issues = []

    with open(filename) as f:
        list_of_lines = f.readlines()
        if list_of_lines[-1][-1] != "\n":
            e = GuidelineDefect(
                    description="Does not have a line-break character at its end.",
                    filename=filename, guideline_reference="2.9")
            issues.append(e.to_string())

    line_number = 0
    for line in list_of_lines:
        line_number += 1
        #print("lint_line({:s})".format(line))
        line_issues = lint_line(line)
        for issue in line_issues:
            # copies the issues reported by lint_line to a list of issues,
            # prepending them with filename and line number:
            issue.filename = filename
            issue.line_number=line_number
            issues.append(issue.to_string())

    return issues


def lint_line(line):
    issues = []
    # 2.1 Code needs to be properly indented
    # (implemented in # 2.3.1, # 2.3.2))

    # 2.2 If a line is too long do not be afraid to break it
    #     (long lines warning at lengths exceeding 120 chars)
    if len(line) > MAX_ACCEPTED_LINE_LENGTH:
        gd = GuidelineDefect("Line is too long ({:d} chars).".format(len(line)),
                guideline_reference="2.2")
        issues.append(gd)

    # 2.3.1 Do not use tabs for indentation.
    #       In fact this boils down to never using tabs at the beginning
    #       of a line (before any non-whitespace character).

    # matches: beginning-of-line, then 0 or more whitespace characters, then a tab character
    for c in re.finditer("^\s*\t", line):
        gd = GuidelineDefect("Tab character(s) used for indentation.",
                guideline_reference="2.3.1")
        issues.append(gd)

    # 2.3.2 Use indentation level of exactly four spaces.
    # @TODO - investigate, probably requires a lexer to know the current indentation level.

    # 2.4 Use spacing when specifying parameters:
    # 2.4.1 Apply spaces after open parentheses
    #       and before closing parentheses
    #       if there is at least one parameter inside these parentheses.
    for c in re.finditer("\([^ \n\)]", line):
        gd = GuidelineDefect("Opening parenthesis is not followed by a space.",
                guideline_reference="2.4.1", char_number=c.start()+1)
        issues.append(gd)
    for c in re.finditer("[^ \(]\)", line):
        gd = GuidelineDefect("Closing parenthesis is not preceded by a space.",
                guideline_reference="2.4.1", char_number=c.start()+1)
        issues.append(gd)

    # 2.4.2 Always put a space character after a comma (except when it's the last character on the line):
    for c in re.finditer(",[^ \n]", line):
        gd = GuidelineDefect("Comma is not followed by a space.",
                guideline_reference="2.4.2", char_number=c.start()+1)
        issues.append(gd)

    # 2.5 Use vertical spacing. (NOT LINTED - _really_ hard to machine-check this)

    # 2.6 Don't overdo the spacing. (NOT LINTED - see above)

    # 2.7 Remember to put the newline character at the end of the file.
    # (checked right after the file is read)

    # 2.8 Write code that is self-documenting. (NOT LINTED)

    # 2.9 Use CamelCase for function names. Use camelCase for variable names.
    # 2.9.1 Use PascalCase for function names
    # @TODO - requires a lexer!

    # 2.9.2 Use camelCase for variable names.
    # @TODO - requires a lexer.

    # 2.10 Variable names should be short, but descriptive. (NOT LINTED)

    # 2.11 Do not make spelling mistakes in variable names, functions and comments. (NOT LINTED)

    # 2.12 Do not use brackets for the outermost level of an if statement.
    # @TODO - requires a lexer.

    # 2.13 Keep functions short and to the point.
    #      Avoid deeply nested logic.
    #      Split functions into multiple smaller ones, if necessary.
    # (NOT LINTED)

    # 2.14 Omit semicolons at the end of statements.
    # @TODO - requires a lexer.

    # 2.15 File names should be consistent with each other. (NOT LINTED)

    return issues
